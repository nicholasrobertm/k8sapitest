require 'kubeclient'

client = Kubeclient::Client.new('http://localhost:8080/api', 'v1')

informer = Kubeclient::Informer.new(client, "certificate", reconcile_timeout: 15 * 60, logger: Logger.new(STDOUT))
informer.start_worker

puts informer.list

informer.list.each do |o|
  puts o.inspect
end